#/bin/bash
#copyright 20022 juanma1980 <juanma1980@disroot.org>
#Licensed under GPL-3
#####
#This a simple install script for testing purposes
#Use at your own risk.  
#
#Parms:
#	--init -> Initialize git repositories
#Usage:
#	./installRebost.sh [--init]
#
#	The script generates an install script that must be executed as root.
#	If the parm --init is present then the script will clone all the 
#	repositories needed in the directory set as GITDIR
#####

GITDIR=$HOME/gitlab_rebost
TMPDIR=/tmp

function cloneRepo
{
	REPO=$1
	cd $GITDIR
	git clone $REPO
	RET=$?
	cd $OLDPWD
	return $RET
}

function updateRepo
{
	REPO=$1
	REPOPATH=$GITDIR/$(basename $repo)
	echo "Updating $REPOPATH"
	cd $REPOPATH || exit 3
	git pull
	RET=$?
	cd $OLDPWD
	return $RET
}
function cloneRepos
{
	OLDPWD=$PWD
	mkdir -p $GITDIR 2>/dev/null
	cd $GITDIR
	repos=(https://github.com/edupals/poinstaller https://gitlab.com/juanma1980/rebost https://gitlab.com/juanma1980/python3-rebost https://gitlab.com/juanma1980/python3-app2menu https://gitlab.com/juanma1980/python3-appconfig https://gitlab.com/juanma1980/epi)
	update=()
	for repo in ${repos[@]}
	do
		cloneRepo $repo
		if [[ $? != 0 ]]
		then
			updateRepo $repo
		fi
	done
	cd $OLDPWD
	echo ""
	echo "Sources available at ${GITDIR}"
}

function updateRepos
{
	OLDPWD=$PWD
	mkdir -p $GITDIR 2>/dev/null
	repos=(https://github.com/edupals/poinstaller https://gitlab.com/juanma1980/rebost https://gitlab.com/juanma1980/python3-rebost https://gitlab.com/juanma1980/python3-app2menu https://gitlab.com/juanma1980/python3-appconfig https://gitlab.com/juanma1980/epi)
	for repo in ${repos[@]}
	do
		REPOPATH=$GITDIR/$(basename $repo)
		echo "Updating $repo"
		cd $REPOPATH || cloneRepo $repo
		updateRepo $repo
	done
	cd $OLDPWD

}

function generateInstallScript
{
	TMPFILE=$(mktemp -p $TMPDIR)
	echo "#!/bin/bash" > $TMPFILE
	echo "#Rebost Installer automated script" >> $TMPFILE
	echo "" >> $TMPFILE
	echo "if [[ \$UID -ne 0 ]]" >> $TMPFILE
	echo "then" >> $TMPFILE
	echo "echo Only root can run this script" >> $TMPFILE
	echo "exit 1" >> $TMPFILE
	echo "fi" >> $TMPFILE
	echo "echo Installing python libraries" >> $TMPFILE
	echo "OLDPWD=$PWD" >> $TMPFILE
	echo "cd $GITDIR" >> $TMPFILE
	echo "for gitrepo in poinstaller python3-app2menu python3-appconfig python3-rebost" >> $TMPFILE
	echo "do" >> $TMPFILE
	echo "cd $GITDIR/\$gitrepo" >> $TMPFILE
	echo "echo Processing \$PWD">> $TMPFILE
	echo "python3 ./setup.py install" >> $TMPFILE
	echo "cd $GITDIR" >> $TMPFILE
	echo "done" >> $TMPFILE
	echo "cd $GITDIR" >> $TMPFILE
	echo "echo Installing EPI installer" >> $TMPFILE
	echo "cd $GITDIR/epi" >> $TMPFILE
	echo "python3 ./setup.py install" >> $TMPFILE
	echo "python3 ./setup_cli.py install" >> $TMPFILE
	echo "python3 ./setup_gtk.py install" >> $TMPFILE
	echo "ln -s epi-gtk/epi-gtk /usr/sbin" >> $TMPFILE
	echo "cd $GITDIR" >> $TMPFILE
	echo "echo Installing rebost security files" >> $TMPFILE
	echo "mkdir -p /etc/polkit-1/localauthority/50-local.d/" >> $TMPFILE
	echo "cd rebost" >> $TMPFILE
	echo "cp dbus/system.d/net.lliurex.rebost.conf /etc/dbus-1/system.d/net.lliurex.rebost.conf" >> $TMPFILE
	echo "cp polkit/net.lliurex.rebost.software.pkla /etc/polkit-1/localauthority/50-local.d/net.lliurex.rebost.software.pkla" >> $TMPFILE
	echo "cp polkit/net.lliurex.rebost.software.policy /usr/share/polkit-1/actions/net.lliurex.rebost.software.policy" >> $TMPFILE
	echo "mkdir -p /usr/share/rebost/helper" >> $TMPFILE
	echo "cp rebost/helper/rebost-software-manager.sh /usr/share/rebost/helper" >> $TMPFILE
	echo "" >> $TMPFILE
	echo "echo Rebost should be installed. Execute rebost-dbus as root from source folder or configure the service to be started at init" >> $TMPFILE
	echo "" >> $TMPFILE
	echo "exit 0" >> $TMPFILE
	chmod +x $TMPFILE
	echo ""
	echo "Install script available at $TMPFILE"
}

function showMsg
{
	echo "Required system dependencies: appstream lib64appstream-glib-gir1.0 lib64appstream-gir1.0 lib64flatpak-gir1.0 (for flatpak) lib64snapd-gir1.0 (for snap)"
	echo "Required python dependencies (pip3 install or through distro repos, usually python3-NAME): html2text beautifulsoup pyside2 notify2"
	echo "When installed run ./rebost-dbus from $GITDIR/rebost/rebost as root or configure as a daemon at startup"
	echo ""

}

function showHelp
{
	echo "
This a simple install script for testing purposes
Use at your own risk.  

Parms:
	--init -> Initialize git repositories
Usage:
	./installRebost.sh [--init]

	The script generates an install script that must be executed as root.
	If the parm --init is present then the script will clone all the 
	repositories needed in the directory set as GITDIR"
	exit 1
}

if [[ $UID == 0 ]]
then
	echo "This script must be not executed as root"
	exit 2
fi

if [[ ! -z $@ ]]
then
	case $1 in
		"--init")
			cloneRepos
			;;
		*)
			showHelp
			;;
	esac
fi
updateRepos
generateInstallScript
showMsg
